node-red-contrib-pisuppy-pijuice-pis0212
=====================

A set of <a href="http://nodered.org">Node-RED</a> nodes to monitor a
PiJuice battery backup system

## Install

First follow the instructions on the Pi Supply PiJuice site.
I enable I2C using sudo raspi-config, in the Interface options.
Note that some battery types can be configured by adjusting
the PiJuice DIP switches or any battery type can be selected
via later software configuration.

I run sudo apt-get install pijuice-base as in my application
the GUI is not required.

After I verify thru testing that the hardware and pijuice_cli
work, then its time to install this Node-RED node.

Either use the Node-RED Menu - Manage Palette option to install,
or run the following command in your Node-RED user
directory - typically `~/.node-red`

        npm i node-red-contrib-pisupply-pijuice-pis0212

The node provides an example flow.  In Node-RED, click the right
side hamburger menu, "import", "Examples",
"node-red-contrib-pisupply-pijuice-pis0212"

I would suggest trying the example demo flow before creating
your own flow.

## Usage

#### Battery Capacity

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric percentage of battery capacity.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of number 97, if the battery
is 97 percent charged.

See help file for more detail.

#### Battery Temperature

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric Celsius temperature of the battery.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of number 25, if the battery
is around 25 degrees C room temperature.

See help file for more detail.

#### Battery Voltage

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric battery voltage in units of Volts.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of number 4.165, if the battery
is nearly fully charged.

See help file for more detail.

#### GPIO Current

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric current in mA on the UPS GPIO pins.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of number 594, implying the Pi is
using 594 mA of current at this instant.

See help file for more detail.

#### GPIO Voltage

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric voltage in Volts on the UPS GPIO pins.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of number 5.039, which is pretty close
to the designed Pi operating voltage of 5 volts.

See help file for more detail.

#### USB Power

 - `msg.payload` - *any*

`msg.payload` any input triggers the start of a poll.

The output payload will be a numeric percentage of battery capacity.

For example, given an input payload of a timestamp, a fraction of a second
later will result in an output payload of string PRESENT, if the USB charge
input has power to charge the battery, or NOT_PRESENT, if there is no or
not enough power at the USB charge input to charge the battery.

See help file for more detail.

## Links

https://uk.pi-supply.com/products/pijuice-standard

https://learn.pi-supply.com/make/pijuice-quick-start-guide-faq/

https://learn.pi-supply.com/make/how-to-save-power-on-your-raspberry-pi/

https://github.com/PiSupply/PiJuice

https://gitlab.com/SpringCitySolutionsLLC/pisupply-pijuice-pis0212

https://www.npmjs.com/package/node-red-contrib-pisupply-pijuice-pis0212

https://flows.nodered.org/node/node-red-contrib-pisupply-pijuice-pis0212

https://nodered.org/

## Copyrights and Licenses

Majority of code Copyright (c) 2020 Spring City Solutions LLC and other
contributors and licensed under the Apache License, Version 2.0

Icon made from http://www.onlinewebfonts.com/icon aka Icon Fonts
is licensed by CC BY 3.0
