#!/usr/bin/env python3
# pijuice-status.py
#
# Links
# https://uk.pi-supply.com/products/pijuice-standard
# https://learn.pi-supply.com/make/pijuice-quick-start-guide-faq/
# https://learn.pi-supply.com/make/how-to-save-power-on-your-raspberry-pi/
# https://github.com/PiSupply/PiJuice
# https://gitlab.com/SpringCitySolutionsLLC/pisupply-pijuice-pis0212
# https://www.npmjs.com/package/node-red-contrib-pisupply-pijuice-pis0212
# https://flows.nodered.org/node/node-red-contrib-pisupply-pijuice-pis0212
# https://nodered.org/
#
# Majority of code Copyright (c) 2020 Spring City Solutions LLC and other
# contributors and licensed under the Apache License, Version 2.0
#
# Need to have previously installed the pijuice-base package
# sudo apt-get install pijuice-base
# And verified successful operation of pijuice_cli
#
# Usage:
#
# pijuice-status.py battery_percent
# returns a decimal percentage of battery charge example "97"
#
# pijuice-status.py battery_temperature
# returns battery temperature in degrees C
#
# pijuice-status.py battery_voltage
# returns battery voltage in volts
#
# pijuice-status.py gpio_current
# returns gpio current in mA
#
# pijuice-status.py gpio_voltage
# returns gpio voltage in volts
#
# pijuice-status.py usb_power
# returns PRESENT or NOT_PRESENT depending on USB charging power presence

import sys

# Previously installed into /usr/lib/python3/dist-packages/pijuice.py
# by running sudo apt-get install pijuice-base
from pijuice import PiJuice, pijuice_hard_functions
from pijuice import pijuice_sys_functions, pijuice_user_functions

BUS = 1
ADDRESS = 0x14
pijuice = PiJuice(BUS, ADDRESS)


def batteryPercent():
    print(pijuice.status.GetChargeLevel().get('data', -1))


def batteryTemperature():
    print(pijuice.status.GetBatteryTemperature().get('data', -999))


def batteryVoltage():
    print(float(pijuice.status.GetBatteryVoltage().get('data', 0)) / 1000)


def gpioCurrent():
    print(float(float(pijuice.status.GetIoCurrent().get('data', 0))))


def gpioVoltage():
    print(float(pijuice.status.GetIoVoltage().get('data', 0)) / 1000)


def usbPower():
    status = pijuice.status.GetStatus().get('data', {})
    usb_power = status.get('powerInput', 0)
    print(str(usb_power))


if (sys.argv[1] == 'battery_percent'):
    batteryPercent()

if (sys.argv[1] == 'battery_temperature'):
    batteryTemperature()

if (sys.argv[1] == 'battery_voltage'):
    batteryVoltage()

if (sys.argv[1] == 'gpio_current'):
    gpioCurrent()

if (sys.argv[1] == 'gpio_voltage'):
    gpioVoltage()

if (sys.argv[1] == 'usb_power'):
    usbPower()
