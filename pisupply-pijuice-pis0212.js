// pisupply-pijuice-pis0212.js
//
// Links
// https://uk.pi-supply.com/products/pijuice-standard
// https://learn.pi-supply.com/make/pijuice-quick-start-guide-faq/
// https://learn.pi-supply.com/make/how-to-save-power-on-your-raspberry-pi/
// https://github.com/PiSupply/PiJuice
// https://gitlab.com/SpringCitySolutionsLLC/pisupply-pijuice-pis0212
// https://www.npmjs.com/package/node-red-contrib-pisupply-pijuice-pis0212
// https://flows.nodered.org/node/node-red-contrib-pisupply-pijuice-pis0212
// https://nodered.org/
//
// Copyright (c) 2020 Spring City Solutions LLC and other contributors
// Licensed under the Apache License, Version 2.0
//
module.exports = function(RED) {
    "use strict";
    var execSync = require('child_process').execSync;
    var fs = require('fs');

    var pijuiceCommand = __dirname + '/pijuice-status.py';
    var allOK = true;

    // the magic to make python print stuff immediately
    process.env.PYTHONUNBUFFERED = 1;

    function batterypercent(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " battery_percent").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("batterypercent out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("batterypercent", batterypercent);

    function batterytemperature(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " battery_temperature").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("batterytemperature out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("batterytemperature", batterytemperature);

    function batteryvoltage(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " battery_voltage").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("batteryvoltage out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("batteryvoltage", batteryvoltage);

    function gpiocurrent(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " gpio_current").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("gpiocurrent out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("gpiocurrent", gpiocurrent);

    function gpiovoltage(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " gpio_voltage").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("gpiovoltage out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("gpiovoltage", gpiovoltage);

    function usbpower(n) {
        RED.nodes.createNode(this, n);
        this.out = n.out || "out";
        var node = this;
        var out = "";

        function inputlistener(msg, send, done) {
            try {
                out = execSync(pijuiceCommand + " usb_power").toString();
            } catch (err) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            }
            if (out < 1) {
                allOK = false;
                node.status({ fill: "red", shape: "ring", text: "pisupply-pijuice-pis0212.status.not-running" });
                RED.log.warn("waveshare-shield-adda-11010 : " + RED._("pisupply-pijuice-pis0212.errors.pijuicecommandfailure"));
            } else {
                msg.payload = out;
                node.send(msg);
                if (RED.settings.verbose) { node.log("usbpower out: " + out); }
                node.status({ fill: "green", shape: "dot", text: out.toString() });
            }
            if (done) { done(); }
        }
        if (allOK === true) {
            node.running = true;
            node.on("input", inputlistener);
        } else {
            node.status({ fill: "grey", shape: "dot", text: "pisupply-pijuice-pis0212.status.not-available" });
            node.on("input", function(msg) {
                node.status({ fill: "grey", shape: "dot", text: RED._("pisupply-pijuice-pis0212.status.na", { value: msg.payload.toString() }) });
            });
        }

        node.on("close", function(done) {
            node.status({ fill: "grey", shape: "ring", text: "pisupply-pijuice-pis0212.status.closed" });
            done();
        });

    }
    RED.nodes.registerType("usbpower", usbpower);

}